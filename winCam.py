'Camera calss for windows implenetation of the Imaging Sourece usb cameras'
import numpy as np
from scipy import ndimage

import ctypes 
import tisgrabber as tis

class winCam():
    
    def __init__(self):
        #load dll for USB camerall
        self.ic = ctypes.cdll.LoadLibrary("./tisgrabber_x64.dll")
        tis.declareFunctions(self.ic)
        self.ic.IC_InitLibrary(0)

        grabbers = []
        
        #get frame grabber and select camera with gui
        self.hGrabber = self.ic.IC_CreateGrabber()
        self.ic.IC_OpenVideoCaptureDevice(self.hGrabber,"DMK 33UX174 1".encode("utf-8"))
        
        self.ic.IC_SetFormat( self.hGrabber, tis.SinkFormats.Y800.value)
        self.ic.IC_SetFrameRate(self.hGrabber, ctypes.c_float(30.0))
        self.ic.IC_SetPropertySwitch(self.hGrabber, tis.T("Exposure"), tis.T("Auto"), 0)
        
        self.ic.IC_StartLive(self.hGrabber, 0)
        if self.ic.IC_IsDevValid(self.hGrabber):
            print('Camera connected')
        
        #!care here when implementing update function
        #variables for image description
        self.imagWidth = ctypes.c_long()
        self.imagHeight = ctypes.c_long()
        imagBitsPerPixel = ctypes.c_int()
        imagColorformat = ctypes.c_int()

        # Query the values of image description
        self.ic.IC_GetImageDescription(self.hGrabber, self.imagWidth, self.imagHeight,
                                    imagBitsPerPixel, imagColorformat)

        # Calculate the buffer size for one image
        self.imageBufferSize = self.imagWidth.value * self.imagHeight.value * imagBitsPerPixel.value

        
    def newImage(self):
        if self.ic.IC_SnapImage(self.hGrabber, 2000) == tis.IC_SUCCESS:
            # Get the image data from frame grabber
            imagePtr = self.ic.IC_GetImagePtr(self.hGrabber)
            imagedata = ctypes.cast(imagePtr, ctypes.POINTER(ctypes.c_ubyte * self.imageBufferSize))
            # Create the numpy array
            image = np.ndarray(buffer=imagedata.contents,
                                    dtype=np.uint8,
                                    shape=(self.imagHeight.value, self.imagWidth.value))
            return image
        else:
            # return image
            #return np.random.normal(128, 64, size=(1200, 1920))
            return np.array(False) 
        
    def stopCam(self):
        #stop live and clos frame grabber
        self.ic.IC_StopLive(self.hGrabber)
        self.ic.IC_ReleaseGrabber(self.hGrabber)

    def setExposure(self, exposure):
        self.ic.IC_SetPropertyAbsoluteValue(self.hGrabber, tis.T("Exposure"), tis.T("Value"), ctypes.c_float(exposure))
        
    def setGain(self, gain):
        self.ic.IC_SetPropertyAbsoluteValue(self.hGrabber, tis.T("Gain"), tis.T("Value"), ctypes.c_float(gain))