#simple software for auto focus using analog level adjustment
import sys
import numpy as np
from scipy import stats
from scipy import ndimage
import time
import serial


from matplotlib.figure import Figure
import matplotlib.pyplot as plt

from matplotlib.backends.backend_qtagg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.backends.backend_qtagg import FigureCanvas

from matplotlib.backends.qt_compat import QtWidgets
from matplotlib.backends.qt_compat import QtCore
from matplotlib.backends.qt_compat import QtGui

import ctypes 
import tisgrabber as tis

from winCam import winCam
from noCam import noCam

NM_PER_PIXEL = 12.33
VOLT_PER_NM = 5e-5
DIVISIONS_PER_VOLT = 1638.5

class Frontend(QtWidgets.QMainWindow):
    
    closeSignal = QtCore.Signal()
    offSetSignal = QtCore.Signal(int)
    gainSignal = QtCore.Signal(float)
    exposureSignal = QtCore.Signal(float)
    focusToggelSignal = QtCore.Signal()
    pixelCalibationSignal = QtCore.Signal()
    
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        #TODO magic numbers
        self.img = np.random.normal(0, 1, size=(400, 640))
        self.focus = 0.0
        self.cameraOffSet = 0
        self.focusOn = False

        self.N = 1
        self.sum = 0
        self.sumSigma = 0
        self.NM_PER_PIXEL = NM_PER_PIXEL
        
        # GUI layout  
        self.main = QtWidgets.QGroupBox("Auto Focus")
        mainPalette = self.main.palette()
        mainPalette.setColor(QtGui.QPalette.ColorRole.Window, QtGui.QColor(139,71,137))
        self.main.setPalette(mainPalette)
        self.setCentralWidget(self.main)
        self.main.setAutoFillBackground(True)
        
        # Main and sub layout
        mainLayout = QtWidgets.QHBoxLayout(self.main)
        leftLayout = QtWidgets.QVBoxLayout()
        rightLayout = QtWidgets.QGridLayout()
        
        # sub sub layout
        buttonLayout = QtWidgets.QHBoxLayout()
        plotBox = QtWidgets.QGroupBox("Camera")
        controlBox = QtWidgets.QGroupBox("Camera control")
        dataBox = QtWidgets.QGroupBox("Focus data")
                   
        # camera image with matplot lib canvas
        self.cameraCanvas = FigureCanvas(plt.figure(figsize=(8, 5)))
        
        #Axes for the camera 
        self.cameraAx = self.cameraCanvas.figure.subplots()
        self.plotImage = self.cameraAx.imshow(self.img, interpolation ='none', cmap='inferno', vmin=0, vmax=255)
        self.center = self.cameraAx.scatter(200,200,marker='x',s=100,c='r')
        self.plotImage.figure.set_layout_engine('tight')
        self.plotImage.figure.set_facecolor((139/256,71/256,137/256))
        self.cameraAx.axis("off")
        
        # TODO fix layout levels
        plotLayout = QtWidgets.QVBoxLayout()
        plotLayout.addWidget(self.cameraCanvas)
        plotLayout.addWidget(NavigationToolbar(self.cameraCanvas, self))
        plotBox.setLayout(plotLayout)
        
        # close button
        self.closeButton = QtWidgets.QPushButton("close")
        self.closeButton.setSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        self.closeButton.clicked.connect(self.endFromGui)

        # focus button
        self.focusButton = QtWidgets.QPushButton("focus")
        self.focusButton.setStyleSheet("background-color : red")
        self.focusButton.setSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        self.focusButton.clicked.connect(self.toggelFocus)

        # cali button
        self.caliButton = QtWidgets.QPushButton("cali")
        self.caliButton.setSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        self.caliButton.clicked.connect(self.startPixelCalibation)

        # focus position text
        self.focusText = QtWidgets.QLabel("000 pixel")
        
        buttonLayout.setDirection(QtWidgets.QHBoxLayout.Direction.RightToLeft)
        buttonLayout.addWidget(self.closeButton)
        buttonLayout.addWidget(self.caliButton)
        buttonLayout.addWidget(self.focusText)
        buttonLayout.addWidget(self.focusButton)
        
        # camera controll 
        exposurButton = QtWidgets.QPushButton("set")
        gainButton = QtWidgets.QPushButton("set")
        offsetButton = QtWidgets.QPushButton("set")

        exposurButton.setCheckable(True)
        exposurButton.clicked.connect(self.exposurClicked)

        gainButton.setCheckable(True)
        gainButton.clicked.connect(self.gainClicked)

        offsetButton.setCheckable(True)
        offsetButton.clicked.connect(self.offSetClicked)
        
        exposurLabel = QtWidgets.QLabel("Exposurtime [ms]")
        gainLabel = QtWidgets.QLabel("Gain [dB]")
        offsetLabel = QtWidgets.QLabel("Offset")
        
        self.exposurSlot = QtWidgets.QLineEdit()
        self.gainSlot = QtWidgets.QLineEdit()
        self.offsetSlot = QtWidgets.QLineEdit()
        
        controlLayout = QtWidgets.QGridLayout()
        controlLayout.addWidget(exposurLabel, 0, 0)
        controlLayout.addWidget(self.exposurSlot, 0, 1)
        controlLayout.addWidget(exposurButton, 0, 2)
        controlLayout.addWidget(gainLabel, 1, 0)
        controlLayout.addWidget(self.gainSlot, 1, 1)
        controlLayout.addWidget(gainButton, 1, 2)
        controlLayout.addWidget(offsetLabel, 2, 0)
        controlLayout.addWidget(self.offsetSlot, 2, 1)
        controlLayout.addWidget(offsetButton, 2, 2)
        
        controlBox.setLayout(controlLayout)
        controlBox.setSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        
        # Auto focus data and button
        self.dataCanvas = FigureCanvas(plt.figure(figsize=(3, 4)))
        self.dataAx = self.dataCanvas.figure.subplots()
        
        self.timeLine = np.linspace(0,100,1000)
        self.dataLine = np.zeros((1000,3))
        
        self.plotList = self.dataAx.plot(self.timeLine, self.dataLine)
        self.plotList[0].set_label('Focus position [pixel/3]')
        self.plotList[1].set_label('Analog input +320')
        self.plotList[2].set_label('Added analog output +320')
        self.dataAx.legend()
        self.t1 = time.time()
        self.startTime = self.t1
        
        dataLayout = QtWidgets.QVBoxLayout()
        dataLayout.addWidget(self.dataCanvas)
        dataLayout.addWidget(NavigationToolbar(self.dataCanvas, self))
        dataBox.setLayout(dataLayout)
        
        leftLayout.addWidget(plotBox)
        rightLayout.addWidget(dataBox,1,0)
        rightLayout.addLayout(buttonLayout,2,0, QtCore.Qt.AlignmentFlag.AlignBottom)
        rightLayout.addWidget(controlBox,0,0, QtCore.Qt.AlignmentFlag.AlignTop)
        
        mainLayout.addLayout(leftLayout,67)
        mainLayout.addLayout(rightLayout,33)
    
    
    'stop backend and send Signal to disconect camera and clos porgram'  
    @QtCore.Slot()    
    def endFromGui(self):      
        #TODO check if thei is the 'right' way to do it
        self.closeSignal.emit()
        time.sleep(1)
        self.close()

    @QtCore.Slot()
    def toggelFocus(self):
        self.N = 1
        self.sum = 0
        self.sumSigma = 0
        if self.focusOn:
            self.focusOn = False
            self.focusButton.setStyleSheet("background-color : red") 
            print('focus off')
        else:
            self.focusOn = True
            self.focusButton.setStyleSheet("background-color : green")        
            print('focus on')
        self.focusToggelSignal.emit()

    @QtCore.Slot()
    def startPixelCalibation(self):
        self.N = 1
        self.sum = 0
        self.sumSigma = 0
        self.focusOn = False
        self.focusButton.setStyleSheet("background-color : red") 
        print('focus off')       
        self.pixelCalibationSignal.emit()

        
    @QtCore.Slot(np.ndarray, np.ndarray)
    def newImage(self, image, cenertOfMass):
        
        if image.size == 0:
            print("Image recived by Slot newImage is empyty")
        else:
            #compress image
            image = image[::3,::3]
            #cenertOfMass /= 3

            #position of backreflection on camera.
            #only take x direction
            self.focus = cenertOfMass[1]

            if self.focusOn:
                self.sum += self.focus
                mu = self.sum/self.N

                self.sumSigma += np.power(self.focus-mu,2)
                self.N += 1
            
            #update image
            self.plotImage.set_data(image)
            #use full range of color map
            #TODO fix pls
            self.plotImage.set_clim([0, image.max()])
            #update center of mass in the image
            self.center.set_offsets(np.flip(cenertOfMass/3))
            self.cameraCanvas.draw()

            # update focus text
            sigma = np.sqrt(self.sumSigma/self.N) * NM_PER_PIXEL
            self.focusText.setText(f'{self.focus:.1f} Pixel RMS: {sigma:.2f} nm')
    
    @QtCore.Slot(float, float)
    def newData(self, analogIn, analogOut):
        
        # update timelin only every 100ms
        # TODO check edge case of time longer than 199 ms
        if time.time() > self.t1 + 0.01:
            #add the new focus position to the ende od the time line
            self.dataLine[:,0] = np.roll(self.dataLine[:,0],-1)
            self.dataLine[-1,0] = self.focus/3

            self.dataLine[:,1] = np.roll(self.dataLine[:,1],-1)
            self.dataLine[-1,1] = analogIn

            self.dataLine[:,2] = np.roll(self.dataLine[:,2],-1)
            self.dataLine[-1,2] = analogOut

            #update time
            self.timeLine += 0.1

            #update data in plot object
            self.plotList[0].set_data(self.timeLine, self.dataLine[:,0])
            self.plotList[1].set_data(self.timeLine, self.dataLine[:,1]+320)
            self.plotList[2].set_data(self.timeLine, self.dataLine[:,2]+320)
            #TODO out of rage focus for limit and magic numbers
            self.dataAx.axis([self.timeLine[0], self.timeLine[-1], 0, 640])
            self.dataCanvas.draw()

    @QtCore.Slot(float)
    def newCali(self, cali):
        self.NM_PER_PIXEL = cali

    #callback for exposure button click
    def exposurClicked(self):
        try:
            exposure = float(self.exposurSlot.text())
            self.exposureSignal.emit(exposure)
        except ValueError:
            print("enter a number in the Exposure field")

    #callback for gain button click
    def gainClicked(self):
        try:
            gain = float(self.gainSlot.text())
            self.gainSignal.emit(gain)
        except ValueError:
            print("enter a number in the Gain field")

    #callback for offset button click            
    def offSetClicked(self):
        try:
            offSet = int(self.offsetSlot.text())
            #prevent overflow of image
            if offSet>=0 & offSet<256:
                self.offSetSignal.emit(offSet)
        except ValueError:
            print("enter a number in the Gain field")


    def makeQtConnection(self, backend):
        backend.newImage.connect(self.newImage)
        backend.newData.connect(self.newData)
        backend.newCali.connect(self.newCali)
        

class Backend(QtCore.QThread):
    #Singnal to be send between worker and gui
    newImage = QtCore.Signal(np.ndarray, np.ndarray)
    newData = QtCore.Signal(float,float)
    newCali = QtCore.Signal(float)
    
    def __init__(self, cameraType):
        super().__init__()
        
        self.running = True
        
        # select approriat camera type
        match cameraType:
            case 'win':
                self.camera = winCam()
            case 'mac':
                print("no mac cam yet")
                sys.exit(0)
            case 'pi':
                print("no rasperry pi cam yet")
                sys.exit(0)
            case 'nocam':
                self.camera = noCam()
            case _:
                print("wrong camera type")
                sys.exit(1)

        #TODO fix amgic numbers
        self.image = np.zeros((1200,1920))

        self.arduino = serial.Serial(port='COM5', baudrate=115200, timeout=.1)
        go = np.int32(0)
        self.arduino.write(go.tobytes())
        self.analogOut = np.int32(0)

        self.cameraOffSet = 0

        self.focusOn = False
        self.targetFocus = 0
        self.focus = 0

        self.pixelCalibration = False
        # start position and stepsize of calibartion in nm
        self.caliStart = -2000
        self.caliStep = 50
        self.caliEnd = 2000
        self.caliI = 0
        self.caliN = int((self.caliEnd - self.caliStart)/self.caliStep)
        self.caliPos = self.caliStart
        self.caliData = np.zeros((2,self.caliN+1))

        self.NM_PER_PIXEL = NM_PER_PIXEL


         
    def run(self):
        #update loop that is running in worker thread
        while self.running:
            self.updateImage()
            self.updateData()
        
    def updateImage(self):
        self.image = self.camera.newImage()
        #apply offset and compensate int overflow
        self.image = self.image.clip(min=self.cameraOffSet)
        self.image = self.image - self.cameraOffSet

        if np.any(self.image):
            #position of backreflection on camera.
            #only take x direction
            cenertOfMass = np.asarray(ndimage.center_of_mass(self.image))
            self.newImage.emit(self.image, cenertOfMass)
            self.focus = cenertOfMass[1]

            if self.focusOn:
                diff = self.targetFocus - self.focus
                # 27 nm/pix 28.03.24 (on the camera)
                #TODO calibation fucntion
                diffNanoMeter = diff * self.NM_PER_PIXEL
                # 0.00005 Volt/nm (piezo controler)
                diffVolt = diffNanoMeter * VOLT_PER_NM
                # 1638.5 divisions/volt (14 bit DAC) with correction factor
                analogDiff = -0.185 * diffVolt * DIVISIONS_PER_VOLT
                # imit for movment in one step to 50 nm
                limit = 50 * VOLT_PER_NM * DIVISIONS_PER_VOLT
                analogDiff = np.clip(analogDiff, -1*limit, limit)
                # send difference output to arduion
                self.analogOut += analogDiff
            else:
                if self.pixelCalibration:
                    #print(f'cali pos: {self.caliPos} and i: {self.caliI}')
                    # get ne pezo position
                    self.analogOut = self.caliPos * VOLT_PER_NM * DIVISIONS_PER_VOLT

                    self.caliData[0,self.caliI] = self.focus
                    self.caliData[1,self.caliI] = self.caliPos
                    # next setp
                    self.caliPos += self.caliStep
                    self.caliI += 1

                    # stop calibation after reaching end point
                    if self.caliPos > self.caliEnd:
                        self.pixelCalibration = False
                        self.caliI = 0
                        self.analogOut = np.int32(0)
                        res = stats.linregress(self.caliData)
                        self.NM_PER_PIXEL = np.fabs(res.slope)
                        print(f'slop is {self.NM_PER_PIXEL:.2f} nm per pixel')
                        self.newCali.emit(self.NM_PER_PIXEL)
                else:
                    self.analogOut = np.int32(0)
        
    def updateData(self):

        output = np.int32(self.analogOut)
        output = np.clip(output, -4096, 4096)
        self.arduino.write(output.tobytes())

        self.data = self.arduino.readline().decode('utf-8')

        self.newData.emit(float(self.data), float(self.analogOut))
        
    @QtCore.Slot()
    def stop(self):
        print('closing backend')
        self.running = False
        #close camera
        self.camera.stopCam()
        self.terminate()

    #@QtCore.Slot()
    def setOffSet(self, offset):
        self.cameraOffSet = offset

    #@QtCore.Slot()
    def setGain(self, gain):
        self.camera.setGain(gain)

    #@QtCore.Slot()
    def setExposure(self, exposure):
        self.camera.setExposure(exposure)

    @QtCore.Slot()
    def toggelFocus(self):
        self.analogOut = np.int32(0)
        if self.focusOn:
            self.focusOn = False
        else:
            self.focusOn = True
            self.targetFocus = self.focus

    @QtCore.Slot()
    def startCali(self):
        print("Starting calibartion")
        self.focusOn = False
        self.pixelCalibration = True
        self.caliPos = self.caliStart
        self.analogOut = self.caliPos * VOLT_PER_NM * DIVISIONS_PER_VOLT
        self.updateData()

        
        
    def makeQtConnection(self, frontend):
        frontend.closeSignal.connect(self.stop)
        frontend.offSetSignal.connect(self.setOffSet)
        frontend.gainSignal.connect(self.setGain)
        frontend.exposureSignal.connect(self.setExposure)
        frontend.focusToggelSignal.connect(self.toggelFocus)
        frontend.pixelCalibationSignal.connect(self.startCali)
        

if __name__ == "__main__":
    
    app = QtWidgets.QApplication(sys.argv)
    
    worker = Backend("win")
    gui = Frontend()
    
    worker.makeQtConnection(gui)
    gui.makeQtConnection(worker)
    
    gui.show()
    worker.start()

    sys.exit(app.exec())