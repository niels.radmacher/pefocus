'Camera calss for testing displays with out any conected cameras'
import numpy as np

class noCam():
    
    def __init__(self):
        self.imageHight = 1200
        self.imageWidth = 1920
        
    def newImage(self):
        # return image
        return np.random.normal(128, 64, size=(self.imageHight, self.imageWidth))
    
    def stopCam(self):
        print('no camera conected closing anyway')