//   (c) 2014
//
//   Karl Bellve
//   Biomedical Imaging Group
//   Molecular Medicine
//   University of Massachusetts Medical School
// 
//   Do not distribute.
//
//   All rights reserved.
//
//   The pgFocus Firmware Source Code is licensed under GPL V3, June 2007.
//
//   http://www.gnu.org/licenses/gpl-3.0.en.html
//
//   You are allowed to modify and use with pgFocus hardware only. 
// 
//   Do Not Distribute the source code. 
//
// Contact: Karl.Bellve@umassmed.edu
//
// http://big.umassmed.edu/wiki/index.php/PgFocus

// include the SPI library:
#include <SPI.h>
#include <Wire.h>
#include <avr/eeprom.h>

#define MAX_DAU 16384
#define MIN_DAU 0
#define MIDDLE_DAU (MAX_DAU/2)
#define MICRONFOCUSADJUST 0.05 // 50 nanometers
#define DAUPERPIXEL 30        // in DAU units 
#define MICRONPERVOLT 10
#define FOCUSDRIFT 20
#define DAUPERVOLT 1638 // Number of DAU /volt (16384/10 Volts = 1638)
#define FIVE_HUNDRED_NM 82 // DAUPERVOLT/MICRONPERVOLT/2 = 81.9
#define REGRESSIONPOINTS 21
#define ADC_TRIGGER 5
#define ADC_PAUSE 50
#define PREFERENCES 1

// DAC
int DAC_SS = A3;
int PD = A2;
int CLR = A1;

// ADC
int ADC_CONVST = A4;
int ADC_BUSY = A5;
int startADC = MIDDLE_DAU, preADC = 0, postADC = 0, diffADC = 0, currentADC = 0;
int skipADC;
double ADC_gain = 1.03458;
unsigned long pauseADC;

int addToAnalog = 0;
int bytesToRead = 4;
byte buffer[4] = {0,0,0,0};


void setup()   {                

  Serial.begin(115200);

  delay (500);

  int time_out = 0;
  //This is blocking...open a serial port to procede...
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
    time_out++;
    if (time_out == 5) break;
    else delay (1000);
  }

  Serial.setTimeout(.1);

  // DAC
  pinMode(DAC_SS, OUTPUT);
  digitalWrite(DAC_SS, HIGH);

  pinMode(PD, OUTPUT);
  digitalWrite(PD, HIGH);

  pinMode(CLR, OUTPUT);
  digitalWrite(CLR, HIGH);

  // ADC
  pinMode(ADC_CONVST, OUTPUT);
  pinMode(ADC_BUSY, INPUT);


  //setDAC(MIDDLE_DAU);
  //startADC = getADC();


  int startTime = millis();

  randomSeed(23); 

  while(!Serial.available()){
    //do nothing
  }
}

void loop(){

  while (!Serial.available());
  Serial.readBytes(buffer, bytesToRead);
  addToAnalog = (int)(buffer[3] << 24) | (int)(buffer[2] << 16) | (int)(buffer[1] << 8) | (int)buffer[0];
  
  //Serial.println(addToAnalog);
  
  Serial.println(getADC());
  setDAC(MIDDLE_DAU-addToAnalog);


}

  


int getADC() 
{ 

  byte lVin = 0;
  byte hVin = 0;
  int volt2;

  SPI.begin();

  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE1); // Maybe use SPI_MODE0
  SPI.setClockDivider(SPI_CLOCK_DIV4);

  digitalWrite(ADC_CONVST, HIGH);
  delayMicroseconds(1);
  digitalWrite(ADC_CONVST, LOW);
  delayMicroseconds(5);

  hVin = SPI.transfer(0); 
  lVin = SPI.transfer(0); 

  digitalWrite(ADC_CONVST, HIGH);  

  volt2  = word(hVin,lVin); 

  // The ADC outputs two's complement
  // Check if the MSB (bit 5) of hVin is high, which means the number is negative and the bits should be flipped, plus 1 added)
  if ((hVin & (1 << 5)) != 0) { 
    volt2 = (~volt2 & 0x3FFF ) + 1; 
    volt2 *= -1; 
    //volt3 |= (1<< 15);
    //volt3 |= (1<< 16);
    //volt3 = ~volt3 + 1;
    //volt3 *= -1;
  } 

  //Serial.print("ADC: ");
  //Serial.println(volt2);

  SPI.end();  
  return(volt2);

}

void setDAC(int DAU) 
{ 
  // 14bit +/- 5Volt

  // Constrain is inclusive 
  //DAU = constrain(DAU,MIN_DAU,MAX_DAU - 1);          

  byte hDAU = highByte(DAU);
  byte lDAU = lowByte(DAU);


  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE2); // maybe SPI_MODE1 if you want clock to be low?
  SPI.setClockDivider(SPI_CLOCK_DIV4);
  delay(1);

  digitalWrite(DAC_SS, LOW);

  SPI.transfer(hDAU);
  SPI.transfer(lDAU); 

  delay(1);
  digitalWrite(DAC_SS, HIGH);

  SPI.end();

}


